﻿using Techniqly.Retrosheet.Utils.Common;

namespace Techniqly.Retrosheet.Utils
{
    public class RetrosheetProcessor : IGameEventProcessor
    {
        private readonly IGameEventProvider _gameEventProvider;
        private readonly IGameEventSink _gameEventSink;

        public RetrosheetProcessor(
            IGameEventProvider gameEventProvider,
            IGameEventSink gameEventSink)
        {
            _gameEventProvider = gameEventProvider;
            _gameEventSink = gameEventSink;
        }

        public void Process()
        {
            var events = _gameEventProvider.GetGameEvents();
            _gameEventSink.WriteGameEvents(events);
        }
    }
}
