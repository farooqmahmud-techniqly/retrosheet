﻿using System.Collections.Generic;
using Techniqly.Retrosheet.Utils.Common.Models;

namespace Techniqly.Retrosheet.Utils.Common
{
    public interface IGameEventProvider
    {
        IEnumerable<GameEvent> GetGameEvents();
    }
}