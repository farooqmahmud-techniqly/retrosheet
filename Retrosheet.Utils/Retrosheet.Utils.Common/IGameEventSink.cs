﻿using System.Collections.Generic;
using Techniqly.Retrosheet.Utils.Common.Models;

namespace Techniqly.Retrosheet.Utils.Common
{
    public interface IGameEventSink
    {
        void WriteGameEvent(GameEvent gameEvent);
        void WriteGameEvents(IEnumerable<GameEvent> gameEvents);
    }
}