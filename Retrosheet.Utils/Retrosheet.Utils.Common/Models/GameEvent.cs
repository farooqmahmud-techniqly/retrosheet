﻿namespace Techniqly.Retrosheet.Utils.Common.Models
{
    /// <summary>
    ///     http://chadwick.sourceforge.net/doc/cwevent.html
    ///     http://www.retrosheet.org/eventfile.htm
    /// </summary>
    public sealed class GameEvent
    {
        public string GameId { get; set; }
        public short YearId { get; set; }
        public string AwayTeam { get; set; }
        public string HomeTeam { get; set; }
        public string BatterId { get; set; }
        public string PitcherId { get; set; }
        public bool BottomOfInning { get; set; }
        public byte EventType { get; set; }
        public decimal WobaPoints { get; set; }
        public uint EventTimestamp { get; set; }
        public byte Inning { get; set; }
        public byte Outs { get; set; }
        public byte AwayTeamScore { get; set; }
        public byte HomeTeamScore { get; set; }
        public char BatterHand { get; set; }
        public char PitcherHand { get; set; }
        public string FirstBaseRunnerId { get; set; }
        public string SecondBaseRunnerId { get; set; }
        public string ThirdBaseRunnerId { get; set; }
        public byte BatterFieldingPosition { get; set; }
        public byte BatterLineupPosition { get; set; }
        public byte RbisOnPlay { get; set; }
        public char? BattedBallType { get; set; }
        public string PitchSequence { get; set; }
        public bool StartOfGame { get; set; }
        public bool EndOfGame { get; set; }
        public byte FirstOutPosition { get; set; }
        public byte SecondOutPosition { get; set; }
        public bool StartOfHalfInning { get; set; }
        public bool EndOfHalfInning { get; set; }
        public byte RunCount { get; set; }
        public byte GamePlateAppearanceCount { get; set; }
        public byte InningPlateAppearanceCount { get; set; }
        public bool PitcherIsStarter { get; set; }
        public byte PlateAppearanceBalls { get; set; }
        public byte PlateAppearanceStrikes { get; set; }
        public byte RunsOnPlay { get; set; }
        public byte BatterFate { get; set; }
        public byte FirstBaseRunnerFate { get; set; }
        public byte SecondBaseRunnerFate { get; set; }
        public byte ThirdBaseRunnerFate { get; set; }
    }
}