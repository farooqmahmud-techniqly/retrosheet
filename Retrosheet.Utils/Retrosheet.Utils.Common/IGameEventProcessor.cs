﻿namespace Techniqly.Retrosheet.Utils.Common
{
    public interface IGameEventProcessor
    {
        void Process();
    }
}